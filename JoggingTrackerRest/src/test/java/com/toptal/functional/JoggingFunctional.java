package com.toptal.functional;

import com.google.common.collect.ImmutableMap;
import com.jayway.restassured.config.JsonConfig;
import com.jayway.restassured.config.RestAssuredConfig;
import com.jayway.restassured.path.json.config.JsonPathConfig;
import com.jayway.restassured.specification.RequestSpecification;
import com.toptal.JoggingTrackerRestApplication;
import com.toptal.models.Jogging;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.List;
import java.util.Map;
import java.util.Set;

import static com.jayway.restassured.RestAssured.given;
import static com.jayway.restassured.RestAssured.port;
import static com.jayway.restassured.http.ContentType.JSON;
import static com.toptal.functional.UtilFunctional.authWhen;
import static org.hamcrest.Matchers.*;

import static org.hamcrest.Matchers.is;

/**
 * Created by dido on 18.01.16.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = JoggingTrackerRestApplication.class)
@WebAppConfiguration
@IntegrationTest("server.port:0")
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@ActiveProfiles("jogging")
public class JoggingFunctional {

    @Value("${local.server.port}")
    private int serverPort;

    private static String sessionId;

    private static final String USERNAME = "joggingUser@gmail.com";
    private static final String PASSWORD = "JOGGINGS";
    private static final String FULLNAME = "TEST TESTOV";
    private static final Integer SIZE = 10;


    @Before
    public void setUp() {

        port = serverPort;
    }

    @Test
    public void test1Register(){
        sessionId = given().contentType(JSON).body(ImmutableMap.of("fullName", FULLNAME, "email", USERNAME, "password", PASSWORD)).when().post("/rest/auth/register").andReturn().sessionId();
    }

    @Test
    public void test2Post(){

        authWhen(sessionId).body(createJogging("2015-12-10","20")).post("/rest/user/joggings").then().body("distance",equalTo(20.0));
        authWhen(sessionId).body(createJogging("2016-01-17","30")).post("/rest/user/joggings").then().body("time",equalTo(3600));
        authWhen(sessionId).body(createJogging("2015-01-18","7")).post("/rest/user/joggings").then().assertThat().body("distance",equalTo(7.0)).assertThat().body("averageSpeed",is("7"));
        authWhen(sessionId).body(createJogging("2015-01-19","5")).post("/rest/user/joggings").then().body("distance",equalTo(5.0));
    }

    @Test
    public void test3Get(){
        authWhen(sessionId).get("/rest/user/joggings").then().assertThat().body("account.id",hasSize(4));
    }

    @Test
    public void test4GetFromTo(){
        authWhen(sessionId).get("/rest/user/joggings?from=2015-01-15&to=2015-01-20").then().assertThat().body("id",hasSize(2));
    }

    @Test
    public void test5GetReport(){
        authWhen(sessionId).get("/rest/user/joggings/report?week=2016-01-15").then().assertThat().body("id",hasSize(1)).body("time",hasItem(3600));
    }

    @Test
    public void test6Delete(){
        List<Integer> joggingsId = authWhen(sessionId).get("/rest/user/joggings").then().extract().path("id");
        authWhen(sessionId).delete("/rest/user/joggings/"+joggingsId.get(0)).then().statusCode(200);
        authWhen(sessionId).get("/rest/user/joggings").then().assertThat().body("id",not(hasItem(joggingsId.get(0))));
    }

    @Test
    public void test7Update(){
        List<Integer> joggingsId = authWhen(sessionId).get("/rest/user/joggings").then().extract().path("id");
        String upDescription = "This is new update run";


        authWhen(sessionId).body(ImmutableMap.of("description",upDescription)).patch("/rest/user/joggings/"+joggingsId.get(0)).then().assertThat().body("description",is(upDescription));
    }

    private static Map<String,String> createJogging(String date,String distance){
        return ImmutableMap.of("description","DESCRIPTION","date",date,"time","3600","distance",distance);
    }


}
