package com.toptal.functional;

import com.google.common.collect.ImmutableMap;
import com.toptal.JoggingTrackerRestApplication;
import org.hamcrest.core.Is;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import static com.jayway.restassured.RestAssured.basic;
import static com.jayway.restassured.RestAssured.given;
import static com.jayway.restassured.RestAssured.port;
import static com.jayway.restassured.http.ContentType.JSON;
import static org.hamcrest.Matchers.is;

/**
 * Created by dido on 18.01.16.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = JoggingTrackerRestApplication.class)
@WebAppConfiguration
@IntegrationTest("server.port:0")
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@ActiveProfiles("auth")
public class AuthenticationFunctional {

    @Value("${local.server.port}")
    private int serverPort;

    private static String sessionId;

    private static final String USERNAME = "testAuth2@gmail.com";
    private static final String PASSWORD = "Authentication";
    private static final String FULLNAME = "TEST TESTOV";

    @Before
    public void setUp() {

        port = serverPort;
    }

    @Test
    public void test1Register(){
        given().contentType(JSON).body(ImmutableMap.of("fullName", FULLNAME, "email", USERNAME, "password", PASSWORD)).when().post("/rest/auth/register").then().assertThat().body("email",is(USERNAME));

    }

    @Test
    public void test2Login(){
        given().contentType(JSON).body(ImmutableMap.of( "email", USERNAME, "password", PASSWORD)).when().post("/rest/auth/login").then().assertThat().body("email",is(USERNAME));

    }

    @Test
    public void test3Logout(){
        String sessionId = given().contentType(JSON).body(ImmutableMap.of("email", USERNAME, "password", PASSWORD)).when().post("/rest/auth/login").andReturn().sessionId();
        given().sessionId(sessionId).contentType(JSON).when().delete("/rest/auth/logout").then().log().all();
        given().contentType(JSON).sessionId(sessionId).when().get("/rest/user").then().statusCode(403);
    }
}
