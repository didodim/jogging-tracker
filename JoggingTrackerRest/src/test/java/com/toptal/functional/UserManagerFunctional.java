package com.toptal.functional;

import com.google.common.collect.ImmutableMap;
import com.toptal.JoggingTrackerRestApplication;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.HashMap;
import java.util.Map;

import static com.jayway.restassured.RestAssured.given;
import static com.jayway.restassured.RestAssured.port;
import static com.jayway.restassured.http.ContentType.JSON;
import static com.toptal.functional.UtilFunctional.authWhen;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;

/**
 * Created by dido on 19.01.16.
 * This is user managment - should be accessable only by admin and user manager
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = JoggingTrackerRestApplication.class)
@WebAppConfiguration
@IntegrationTest("server.port:0")
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@ActiveProfiles("user-manager")
public class UserManagerFunctional {

    private static final String USERNAME = "normalUser@gmail.com";
    private static final String USERNAME2 = "userManager@gmail.com";
    private static final String PASSWORD = "1234";
    private static final String FULLNAME = "TEST TESTOV";

    @Value("${local.server.port}")
    private int serverPort;

    @Value("${jogging.admin.email}")
    private String emailAdmin;

    @Value("${jogging.admin.password}")
    private String passwordAdmin;

    private static String sessionAdminId;

    private static String sessionUserManagerId;
    private static String sessionUserId;

    private static Map user;

    private static Map userManager;

    @Before
    public void setUp() {

        port = serverPort;
    }

    @Test
    public void test0RegisterAdmin(){
        given().contentType(JSON).when().get("/rest/auth/register-admin").then().statusCode(200);
    }

    @Test
    public void test1Register(){
        userManager = (Map) given().contentType(JSON).body(ImmutableMap.of("fullName", FULLNAME, "email", USERNAME2, "password", PASSWORD)).when().post("/rest/auth/register").then().extract().jsonPath().getMap("");
        user = (Map) given().contentType(JSON).body(ImmutableMap.of("fullName", FULLNAME, "email", USERNAME, "password", PASSWORD)).when().post("/rest/auth/register").then().extract().jsonPath().getMap("");

    }
    @Test
    public void test2AdminLogin(){
        sessionAdminId = given().contentType(JSON).body(ImmutableMap.of( "email", emailAdmin, "password", passwordAdmin)).when().post("/rest/auth/login").andReturn().sessionId();
        sessionUserId = given().contentType(JSON).body(ImmutableMap.of( "email", USERNAME, "password", PASSWORD)).when().post("/rest/auth/login").andReturn().sessionId();
    }

    @Test
    public void test3PromoteManager(){
        authWhen(sessionAdminId).get("/rest/admin/promote?email="+userManager.get("email")+"&roleType=ROLE_USER_MANAGER").then().statusCode(200);
    }

    @Test
    public void test4LoginUserManager(){
        sessionUserManagerId = given().contentType(JSON).body(ImmutableMap.of( "email", USERNAME2, "password", PASSWORD)).when().post("/rest/auth/login").andReturn().sessionId();
      }



    @Test
    public void test5GetAccountsFromAdmin(){

        authWhen(sessionUserManagerId).get("/rest/accounts").then().statusCode(403);
    }

    @Test
    public void test5DeleteFromAdmin(){
        authWhen(sessionUserManagerId).contentType(JSON).when().delete("/rest/accounts/"+user.get("id")).then().statusCode(403);
    }

    @Test
    public void test6GetAccounts(){
        authWhen(sessionUserManagerId).get("/rest/manager/accounts").then().body("",hasSize(3));
    }

    @Test
    public void test6GetAccountsAsUser(){
        authWhen(sessionUserId).get("/rest/manager/accounts").then().statusCode(403);
    }

    @Test
    public void test7AddAccounts(){
        authWhen(sessionUserManagerId).body(ImmutableMap.of("email","dido@gm.com","password","123456","fullName","Deyan")).post("/rest/manager/accounts").then().body("email",is("dido@gm.com"));
    }

    @Test
    public void test7UpAccounts(){
        authWhen(sessionUserManagerId).body(ImmutableMap.of("fullName","New Name")).patch("/rest/manager/accounts/"+user.get("id")).then().body("fullName",is("New Name"));
    }

    @Test
    public void test8DeleteAdmin(){
        authWhen(sessionUserManagerId).contentType(JSON).when().delete("/rest/manager/accounts/1").then().body("message",is("Cannot delete admin!")).and().statusCode(500);
    }

    @Test
    public void test8DeleteUser(){
        authWhen(sessionUserManagerId).contentType(JSON).when().delete("/rest/manager/accounts/"+user.get("id")).then().statusCode(200);
    }


}
