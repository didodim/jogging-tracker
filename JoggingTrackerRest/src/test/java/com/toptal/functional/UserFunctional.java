package com.toptal.functional;

import com.google.common.collect.ImmutableMap;
import com.jayway.restassured.config.JsonConfig;
import com.jayway.restassured.config.RestAssuredConfig;
import com.jayway.restassured.path.json.config.JsonPathConfig;
import com.jayway.restassured.specification.RequestSpecification;
import com.toptal.JoggingTrackerRestApplication;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import static com.jayway.restassured.RestAssured.*;
import static com.jayway.restassured.http.ContentType.*;
import static com.toptal.functional.UtilFunctional.authWhen;
import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.core.Is.is;

/**
 * Created by dido on 18.01.16.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = JoggingTrackerRestApplication.class)
@WebAppConfiguration
@IntegrationTest("server.port:0")
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@ActiveProfiles("user")
public class UserFunctional {

    @Value("${local.server.port}")
    private int serverPort;

    private static final String USERNAME = "test@gmail.com";
    private static final String PASSWORD = "1234";
    private static final String FULLNAME = "TEST TESTOV";
    private static String sessionId;



    @Before
    public void setUp() {
        basic(USERNAME,PASSWORD);

        port = serverPort;
    }

    @Test
    public void test1Register(){
        sessionId = given().contentType(JSON).body(ImmutableMap.of("fullName", FULLNAME, "email", USERNAME, "password", PASSWORD)).when().post("/rest/auth/register").andReturn().sessionId();

    }

    @Test
    public void test2User(){
        authWhen(sessionId).get("/rest/user").then().assertThat().body("email",is(USERNAME)).and().body("fullName",is(FULLNAME));

    }

    @Test
    public void test3Roles(){
        authWhen(sessionId).get("/rest/user/roles").then().assertThat().body("authority",hasItem("ROLE_USER"));

    }




}
