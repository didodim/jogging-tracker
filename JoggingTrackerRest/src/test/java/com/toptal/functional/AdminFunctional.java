package com.toptal.functional;

import com.google.common.collect.ImmutableMap;
import com.toptal.JoggingTrackerRestApplication;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import static com.jayway.restassured.RestAssured.given;
import static com.jayway.restassured.RestAssured.port;
import static com.jayway.restassured.http.ContentType.JSON;
import static com.toptal.functional.UtilFunctional.authWhen;
import static org.hamcrest.Matchers.*;

/**
 * Created by dido on 19.01.16.
 * This is user managment - should be accessable only by admin and user manager
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = JoggingTrackerRestApplication.class)
@WebAppConfiguration
@IntegrationTest("server.port:0")
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@ActiveProfiles("admin")
public class AdminFunctional {


    private static final String USERNAME_MANAGER = "test2321@gmail.com";
    private static final String USERNAME2 = "testtest@gmail.com";
    private static final String PASSWORD = "1234";
    private static final String FULLNAME = "TEST TESTOV";

    @Value("${local.server.port}")
    private int serverPort;

    @Value("${jogging.admin.email}")
    private String emailAdmin;

    @Value("${jogging.admin.password}")
    private String passwordAdmin;

    private static String sessionId;

    private static Integer userId;

    @Before
    public void setUp() {

        port = serverPort;
    }

    @Test
    public void test1RegisterAdmin(){
        given().contentType(JSON).when().get("/rest/auth/register-admin").then().statusCode(200);
    }

    @Test
    public void test1Register(){
        given().contentType(JSON).body(ImmutableMap.of("fullName", FULLNAME, "email", USERNAME_MANAGER, "password", PASSWORD)).when().post("/rest/auth/register").then().statusCode(200);
        userId = given().contentType(JSON).body(ImmutableMap.of("fullName", FULLNAME, "email", USERNAME2, "password", PASSWORD)).when().post("/rest/auth/register").then().extract().path("id");
    }



    @Test
    public void test2AdminLogin(){
       sessionId = given().contentType(JSON).body(ImmutableMap.of( "email", emailAdmin, "password", passwordAdmin)).when().post("/rest/auth/login").andReturn().sessionId();

    }
    @Test
    public void test3PromoteManager(){
        authWhen(sessionId).get("/rest/admin/promote?email="+USERNAME_MANAGER+"&roleType=ROLE_USER_MANAGER").then().statusCode(200);
    }

    @Test
    public void test3PromoteAdmin(){
        authWhen(sessionId).get("/rest/admin/promote?email="+USERNAME_MANAGER+"&roleType=ROLE_ADMIN").then().statusCode(200);
    }

    @Test
    public void test4DemoteAdmin(){
        authWhen(sessionId).get("/rest/admin/demote?email="+USERNAME_MANAGER+"&roleType=ROLE_ADMIN").then().statusCode(200);
        String userSessionId = given().contentType(JSON).body(ImmutableMap.of("fullName", FULLNAME, "email", USERNAME_MANAGER, "password", PASSWORD)).when().post("/rest/auth/login").andReturn().sessionId();
        authWhen(userSessionId).get("/rest/user/roles").then().assertThat().body("authority",not(hasItem("ROLE_ADMIN")));
    }

    @Test
    public void test5GetAccounts(){
        authWhen(sessionId).get("/rest/accounts/").then().assertThat().body("_embedded.accounts.email",hasSize(3));
    }

    @Test
    public void test5GetUnauthorized(){
        given().contentType(JSON).when().get("/rest/accounts/").then().statusCode(403);
    }

    @Test
    public void test6DeleteUnauthorized(){
        given().contentType(JSON).when().delete("/rest/accounts/"+userId).then().statusCode(403);
    }

    @Test
    public void test6Delete(){
        authWhen(sessionId).contentType(JSON).when().delete("/rest/accounts/"+userId).then().statusCode(204);
    }

}
