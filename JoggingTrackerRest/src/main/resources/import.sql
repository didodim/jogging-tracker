Insert into roles(id,role_type) SELECT 0,'ROLE_USER' WHERE NOT EXISTS (SELECT id FROM roles WHERE id = 0);
Insert into roles(id,role_type)  SELECT 1,'ROLE_USER_MANAGER' WHERE NOT EXISTS (SELECT id FROM roles WHERE id = 1);
Insert into roles(id,role_type)  SELECT 2,'ROLE_ADMIN' WHERE NOT EXISTS (SELECT id FROM roles WHERE id = 2);
COMMIT ;
