package com.toptal.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by dido on 07.01.16.
 */
@Entity
@Table(name="ROLES")
public class Role {


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column(name = "role_type", unique = true)
    @Enumerated(EnumType.STRING)
    private RoleType roleType;

    @OneToMany(mappedBy = "role")
    @JsonIgnore
    private Set<AccountRole> roleAccounts = new HashSet<>();

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public RoleType getRoleType() {
        return roleType;
    }

    public void setRoleType(RoleType roleType) {
        this.roleType = roleType;
    }

    public Set<AccountRole> getRoleAccounts() {
        return roleAccounts;
    }

    public void setRoleAccounts(Set<AccountRole> roleAccounts) {
        this.roleAccounts = roleAccounts;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Role role = (Role) o;

        if (id != role.id) return false;
        return roleType == role.roleType;

    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (roleType != null ? roleType.hashCode() : 0);
        return result;
    }
}