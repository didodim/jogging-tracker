package com.toptal.models;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.util.Collection;

/**
 * Created by dido on 07.01.16.
 */
public class AccountDetail extends User {
    private Account account;

    public AccountDetail(Account account, Collection<GrantedAuthority> grantedAuthorities) {
        super(account.getEmail(), account.getPassword(), grantedAuthorities);

        this.account = account;
    }

    public Account getAccount() {
        return account;
    }
}