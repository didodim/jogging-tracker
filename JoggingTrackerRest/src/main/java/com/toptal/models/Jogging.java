package com.toptal.models;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.annotations.Type;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;


/**
 * Created by dido on 15.01.16.
 */
@Entity
@Table(name="JOGGINGS")
public class Jogging {

    @Version
    private Long version;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne
    @NotNull
    private Account account;

    @Column
    @NotNull
    private Date date;

    //Time in seconds
    @Column
    @NotNull
    private Long time;

    //Distance in kilometers
    @Column
    @NotNull
    private Double distance;


    @Column
    @NotNull
    private String description;

//Km per hour
    public String getAverageSpeed(){
       Double hours =  time.doubleValue() / 3600;
        DecimalFormat df = new DecimalFormat("#.##");
        return df.format(distance/hours);
    }

    public String getFormattedDate(){
        return new SimpleDateFormat("yyyy-MM-dd").format(date);
        //return DateFormat.getDateInstance(DateFormat.DEFAULT).format(date);
    }

    public long getId() {
        return id;
    }

    public Date getDate() {
        return date;
    }


    public void setDate(Date date) {
        this.date = date;
    }


    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getTime() {
        return time;
    }

    public void setTime(Long time) {
        this.time = time;
    }

    public Double getDistance() {
        return distance;
    }

    public void setDistance(Double distance) {
        this.distance = distance;
    }


    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (!(o instanceof Jogging))
            return false;

        Jogging jogging = (Jogging) o;

        return id == jogging.id;

    }


    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
