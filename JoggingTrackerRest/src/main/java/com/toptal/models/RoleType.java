package com.toptal.models;

/**
 * Created by dido on 07.01.16.
 */
public enum RoleType {
    ROLE_USER, ROLE_USER_MANAGER, ROLE_ADMIN;
}
