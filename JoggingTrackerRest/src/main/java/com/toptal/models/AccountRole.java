package com.toptal.models;


import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * Created by dido on 13.01.16.
 */
@Entity
@Table(name = "accounts_roles")
public class AccountRole {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @ManyToOne
    @JoinColumn(name = "ACCOUNT_ID")
    @NotNull
    private Account account;

    @ManyToOne
    @JoinColumn(name = "ROLE_ID")
    @NotNull
    private Role role;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }


    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }


    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AccountRole that = (AccountRole) o;

        return id == that.id;

    }

    @Override
    public int hashCode() {
        int result = account.hashCode();
        result = 31 * result + role.hashCode();
        return result;
    }
}
