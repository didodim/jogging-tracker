package com.toptal.models;

import com.fasterxml.jackson.annotation.*;

import org.hibernate.validator.constraints.Email;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by dido on 07.01.16.
 */
@Entity
@Table(name="ACCOUNTS")
public class Account {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL, mappedBy="account")
    private Set<Jogging> joggings;

    @Column(name = "email",unique = true)
    @NotNull
    @Email
    private String email;

    @NotNull
    @Size(min = 4)
    @Column(name = "password",updatable = false)
    @JsonIgnore
    private String password;



    @NotNull
    @Column
    private String fullName;

    @OneToMany(mappedBy = "account", cascade = CascadeType.ALL,orphanRemoval=true)
    @JsonIgnore
    private Set<AccountRole> accountRoles = new HashSet<>();

    public Set<AccountRole> getAccountRoles() {
        return accountRoles;
    }

    public void setAccountRoles(Set<AccountRole> accountRoles) {
        this.accountRoles = accountRoles;
    }

    public Long getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @JsonIgnore
    public String getPassword() {
        return password;
    }

    @JsonProperty
    public void setPassword(String password) {
        this.password = password;
    }


    public void setId(Long id) {
        this.id = id;
    }



    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }



}
