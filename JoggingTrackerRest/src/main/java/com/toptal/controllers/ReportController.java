package com.toptal.controllers;

import com.toptal.models.AccountDetail;
import com.toptal.models.Jogging;
import com.toptal.services.JoggingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * Created by dido on 18.01.16.
 */
@RestController
@RequestMapping(value = "/user/joggings/report")
public class ReportController {

    @Autowired
    JoggingService joggingService;

    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    public List<Jogging> weekReport(@AuthenticationPrincipal AccountDetail accountDetail, @RequestParam(required = false) @DateTimeFormat(pattern="yyyy-MM-dd") Date week){

        return joggingService.weekReport(accountDetail.getAccount(),week);

    }
}
