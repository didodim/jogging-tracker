package com.toptal.controllers;

import com.toptal.models.Account;
import com.toptal.models.AccountDetail;
import com.toptal.repositories.AccountRepository;
import com.toptal.repositories.RoleRepository;
import com.toptal.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.RepositoryLinksResource;
import org.springframework.hateoas.ResourceProcessor;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

/**
 * Created by dido on 11.01.16.
 */
@RestController
@RequestMapping(value = "/user")
public class UserController implements ResourceProcessor<RepositoryLinksResource> {

    @Autowired
    AccountRepository accountRepository;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    UserService userService;

    @RequestMapping(value = "", method = RequestMethod.GET)
    @ResponseBody
    public Account get(@AuthenticationPrincipal AccountDetail accountDetail){
        return accountDetail.getAccount();
    }

    @RequestMapping(value = "", method = RequestMethod.PATCH)
    @ResponseBody
    public Account update(@AuthenticationPrincipal AccountDetail accountDetail,@RequestBody Account account){

        return userService.update(account, accountDetail.getAccount().getId());

    }

    @RequestMapping(value = "roles", method = RequestMethod.GET)
    @ResponseBody
    public Collection<GrantedAuthority> roles(@AuthenticationPrincipal AccountDetail accountDetail){
        return accountDetail.getAuthorities();

    }


    /**
     * This method add the controller path to resources
     * @param resource
     * @return
     */
    @Override
    public RepositoryLinksResource process(RepositoryLinksResource resource) {
        resource.add(ControllerLinkBuilder.linkTo(UserController.class).withRel("user"));
        return resource;
    }

}
