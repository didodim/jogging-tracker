package com.toptal.controllers;

import com.toptal.models.AccountDetail;
import com.toptal.models.Jogging;
import com.toptal.repositories.JoggingRepository;
import com.toptal.services.JoggingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.Set;

/**
 * Created by dido on 1/16/16.
 *
 * Jogging only by user access
 */
@RestController
@RequestMapping(value = "/user/joggings")
public class JoggingController {

	@Autowired
	JoggingService joggingService;


	@RequestMapping(method = RequestMethod.GET)
	@ResponseBody
	public Set<Jogging> joggings(@AuthenticationPrincipal AccountDetail accountDetail,@RequestParam(required = false) @DateTimeFormat(pattern="yyyy-MM-dd")  Date from,@RequestParam(required = false) @DateTimeFormat(pattern="yyyy-MM-dd")  Date to){
		if(from == null && to == null){
			return joggingService.getByUser(accountDetail.getAccount());
		}

		return joggingService.findByFromTo(accountDetail.getAccount(),from,to);

	}

	@RequestMapping( method = RequestMethod.POST)
	@ResponseBody
	public Jogging joggingsPost(@AuthenticationPrincipal AccountDetail accountDetail, @RequestBody  Jogging jogging){

		return joggingService.save(jogging,accountDetail.getAccount());

	}

	@RequestMapping(value = "{id}", method = RequestMethod.DELETE)
	@ResponseBody
	public Boolean delete(@PathVariable Long id,@AuthenticationPrincipal AccountDetail accountDetail){

		joggingService.deleteForUser(id,accountDetail.getAccount());
		return true;

	}

	@RequestMapping(value = "{id}", method = RequestMethod.PATCH)
	@ResponseBody
	public Jogging update(@PathVariable Long id,@AuthenticationPrincipal AccountDetail accountDetail, @RequestBody Jogging jogging){
		return joggingService.update(id,jogging,accountDetail.getAccount());
	}
}
