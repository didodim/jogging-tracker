package com.toptal.controllers;

import com.toptal.exceptions.AuthenticationFailException;
import com.toptal.models.AccountDetail;
import com.toptal.models.Role;
import com.toptal.models.RoleType;
import com.toptal.services.AdminService;
import com.toptal.services.AuthenticationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import javax.xml.ws.Response;
import java.util.Set;

/**
 * Created by dido on 13.01.16.
 */
@RestController
@RequestMapping(value = "/admin")
@PreAuthorize("hasRole('ADMIN')")
public class AdminController {

    @Autowired
    AdminService adminService;

    @RequestMapping(value = "promote", method = RequestMethod.GET)
    @ResponseBody
    public Boolean giveRole(@RequestParam String email, @RequestParam RoleType roleType) throws AuthenticationFailException {
        adminService.promote(email,roleType);
        return true;
    }

    @RequestMapping(value = "demote", method = RequestMethod.GET)
    @ResponseBody
    public Boolean removeRole(@AuthenticationPrincipal AccountDetail accountDetail,@RequestParam String email, @RequestParam RoleType roleType) throws AuthenticationFailException {
        adminService.decrease(email,roleType);
        return true;
    }

    @RequestMapping(value = "rolesByEmail", method = RequestMethod.GET)
    @ResponseBody
    public Set<Role> rolesByEmail(@RequestParam String email) throws AuthenticationFailException {
        return adminService.rolesByEmail(email);

    }
}
