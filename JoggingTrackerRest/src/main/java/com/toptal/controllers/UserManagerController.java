package com.toptal.controllers;

import com.toptal.exceptions.AuthenticationFailException;
import com.toptal.models.Account;
import com.toptal.models.AccountDetail;
import com.toptal.repositories.AccountRepository;
import com.toptal.services.UserManagerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

/**
 * Created by dido on 19.01.16.
 */
@RestController
@RequestMapping(value = "/manager/accounts")
public class UserManagerController {

    @Autowired
    AccountRepository accountRepository;

    @Autowired
    UserManagerService userManagerService;

    @RequestMapping(value = "", method = RequestMethod.GET)
    @ResponseBody
    public Iterable<Account> get(@AuthenticationPrincipal AccountDetail accountDetail){
        return accountRepository.findAll();
    }

    @RequestMapping(value = "", method = RequestMethod.POST)
    @ResponseBody
    public Account post(@AuthenticationPrincipal AccountDetail accountDetail,@RequestBody  Account account){
        account.setId(null);

        return accountRepository.save(account);

    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PATCH)
    @ResponseBody
    public Account update(@PathVariable Long id, @AuthenticationPrincipal AccountDetail accountDetail,@RequestBody Account account) throws AuthenticationFailException {
        account.setId(id);
        return userManagerService.update(account);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public Boolean delete(@PathVariable Long id, @AuthenticationPrincipal AccountDetail accountDetail) throws AuthenticationFailException {
        userManagerService.delete(id);
        return true;
    }
}
