package com.toptal.controllers;

import com.toptal.exceptions.AuthenticationFailException;
import com.toptal.models.Account;
import com.toptal.models.AccountDetail;
import com.toptal.models.RoleType;
import com.toptal.services.AuthenticationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.RepositoryLinksResource;
import org.springframework.hateoas.ResourceProcessor;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.util.comparator.BooleanComparator;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * Created by dido on 07.01.16.
 */
@RestController
@RequestMapping(value = "/auth")
public class AuthenticationController implements ResourceProcessor<RepositoryLinksResource> {

    @Autowired
    AuthenticationService authenticationService;

    @RequestMapping(value = "register", method = RequestMethod.POST)
    @ResponseBody
    public Account register(@RequestBody @Validated Account user){
        return authenticationService.register(user);
    }

    @RequestMapping(value = "login", method = RequestMethod.POST)
    @ResponseBody
    public Account login(@RequestBody Account user) throws AuthenticationFailException {
        return authenticationService.login(user);
    }

    @RequestMapping(value = "logout", method = RequestMethod.DELETE)
    @ResponseBody
    public Boolean login() throws AuthenticationFailException {
        authenticationService.logout();
        return true;
    }


    @RequestMapping(value = "register-admin", method = RequestMethod.GET)
    @ResponseBody
    public Boolean registerDefaultAdmin() throws AuthenticationFailException {
        authenticationService.registerDefaultAdmin();
        return true;
    }


    /**
     * This method add the controller path to resources
     * @param resource
     * @return
     */
    @Override
    public RepositoryLinksResource process(RepositoryLinksResource resource) {
        resource.add(ControllerLinkBuilder.linkTo(AuthenticationController.class).withRel("auth"));
        return resource;
    }

}
