package com.toptal.services;

import com.toptal.exceptions.AuthenticationFailException;
import com.toptal.models.Account;
import com.toptal.models.AccountRole;
import com.toptal.models.RoleType;
import com.toptal.repositories.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Set;

/**
 * Created by dido on 19.01.16.
 */
@Service
public class UserManagerService {

    @Autowired
    AccountRepository accountRepository;

    public void delete(Long id) throws AuthenticationFailException {
        Account account = accountRepository.findOne(id);
        isAdmin(account.getAccountRoles());
        accountRepository.delete(id);

    }

    private void isAdmin(Set<AccountRole> accountRoleSet) throws AuthenticationFailException {
        for (AccountRole accountRole : accountRoleSet) {
            if(accountRole.getRole().getRoleType().equals(RoleType.ROLE_ADMIN))
                throw new AuthenticationFailException("Cannot delete admin!");
        }
    }

    public Account update(Account account) throws AuthenticationFailException {
        Account oldAccount = accountRepository.findOne(account.getId());
        isAdmin(oldAccount.getAccountRoles());
        if(account.getFullName() != null){
            oldAccount.setFullName(account.getFullName());
        }
        if(account.getEmail() != null){
            oldAccount.setEmail(account.getEmail());
        }
        return accountRepository.save(oldAccount);

    }
}
