package com.toptal.services;

import com.toptal.exceptions.AuthenticationFailException;
import com.toptal.models.*;
import com.toptal.repositories.AccountRepository;
import com.toptal.repositories.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Service;

import java.util.Collection;

/**
 * Created by dido on 07.01.16.
 */
@Service
public class AuthenticationService {

    @Autowired
    AccountRepository accountRepository;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    AccountDetailsService accountDetailsService;

    @Autowired
    AuthenticationManager authenticationManager;

    @Value("${jogging.admin.email}")
    String defaultAdminEmail;

    @Value("${jogging.admin.password}")
    String defaultAdminPass;



    public Account register(Account account) {
        Role userRole = roleRepository.findByRoleType(RoleType.ROLE_USER);

        AccountRole accountRole = new AccountRole();
        accountRole.setAccount(account);
        accountRole.setRole(userRole);
        account.getAccountRoles().add(accountRole);
        accountRepository.save(account);
        authenticate(account);

        return account;
    }

    public Account registerAdmin(Account account) {
        Iterable<Role> allRoles = roleRepository.findAll();
        allRoles.forEach(role ->{
            AccountRole accountRole = new AccountRole();
            accountRole.setAccount(account);
            accountRole.setRole(role);
            account.getAccountRoles().add(accountRole);
        });
        accountRepository.save(account);
        return account;
    }


    public void registerDefaultAdmin() throws AuthenticationFailException {
        Account byEmail = accountRepository.findByEmail(defaultAdminEmail);
        if(byEmail == null){
            Account admin = new Account();
            admin.setEmail(defaultAdminEmail);
            admin.setPassword(defaultAdminPass);
            admin.setFullName("Default Admin");
            registerAdmin(admin);
            return;
        }
        throw new AuthenticationFailException("Default admin is registered");

    }

    public void authenticate(Account account){
        Collection<GrantedAuthority> grantedAuthorities = accountDetailsService.generateGrantedAuthorities(account);
        User user = new AccountDetail(account, grantedAuthorities);
        Authentication auth = new UsernamePasswordAuthenticationToken(user, user.getPassword(), user.getAuthorities());
        SecurityContextHolder.getContext().setAuthentication(auth);

    }

    public Account login(Account account) throws AuthenticationFailException {
        Account byEmail = accountRepository.findByEmail(account.getEmail());
        if(byEmail == null){
            throw new AuthenticationFailException("Username is invalid");
        }
        if(account.getPassword().equals(byEmail.getPassword())){
            authenticate(byEmail);
            return byEmail;
        }
        throw new AuthenticationFailException("Password is invalid");
    }




    public void logout(){
        SecurityContextHolder.clearContext();
    }

}
