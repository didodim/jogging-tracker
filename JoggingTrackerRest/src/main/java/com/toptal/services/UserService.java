package com.toptal.services;

import com.toptal.models.Account;
import com.toptal.repositories.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by dido on 1/19/16.
 */
@Service
public class UserService {

	@Autowired
	AccountRepository accountRepository;

	@Autowired
	AuthenticationService authenticationService;

	public Account update(Account account, Long id){
		Account old = accountRepository.findOne(id);
		if (account.getEmail() != null){
			old.setEmail(account.getEmail());
		}

		if (account.getFullName() != null){
			old.setFullName(account.getFullName());
		}
		authenticationService.authenticate(old);
		return accountRepository.save(old);


	}
}
