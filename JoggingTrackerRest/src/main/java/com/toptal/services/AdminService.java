package com.toptal.services;

import com.toptal.models.Account;
import com.toptal.models.AccountRole;
import com.toptal.models.Role;
import com.toptal.models.RoleType;
import com.toptal.repositories.AccountRepository;
import com.toptal.repositories.AccountRoleRepository;
import com.toptal.repositories.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by dido on 13.01.16.
 */
@Service
public class AdminService {

    @Autowired
    AccountRepository accountRepository;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    AccountRoleRepository accountRoleRepository;



    public void promote(String email, RoleType roleType) {
        Account byEmail = accountRepository.findByEmail(email);
        Role byRoleType = roleRepository.findByRoleType(roleType);
        AccountRole accountRole = new AccountRole();
        accountRole.setAccount(byEmail);
        accountRole.setRole(byRoleType);
        byEmail.getAccountRoles().add(accountRole);
        accountRepository.save(byEmail);

    }


    public void decrease(String email, RoleType roleType) {
        Account byEmail = accountRepository.findByEmail(email);
        AccountRole ar = null;
        for (AccountRole accountRole : byEmail.getAccountRoles()) {
            if(accountRole.getRole().getRoleType().equals(roleType)){
                ar = accountRole;
            }
        }
        if(ar != null){
            byEmail.getAccountRoles().remove(ar);
        }

        accountRepository.save(byEmail);
    }

    public Set<Role> rolesByEmail(String email) {
        Account byEmail = accountRepository.findByEmail(email);
        return byEmail.getAccountRoles().stream().map(ar -> ar.getRole()).collect(Collectors.toSet());

    }
}
