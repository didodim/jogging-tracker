package com.toptal.services;

import com.toptal.models.Account;
import com.toptal.models.AccountDetail;
import com.toptal.repositories.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.stream.Collectors;

/**
 * Created by dido on 07.01.16.
 */
@Service
public class AccountDetailsService implements UserDetailsService {

    @Autowired
    private AccountRepository accountRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Account account = accountRepository.findByEmail(username);
        if(account == null){
            throw new UsernameNotFoundException("User not found");
        }
        return new AccountDetail(account, generateGrantedAuthorities(account));
    }

    public Collection<GrantedAuthority> generateGrantedAuthorities(Account account){
        return account.getAccountRoles().stream().map(accountRole -> (GrantedAuthority) () -> accountRole.getRole().getRoleType().toString()).collect(Collectors.toList());
    }
}
