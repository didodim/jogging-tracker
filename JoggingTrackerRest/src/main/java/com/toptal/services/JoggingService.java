package com.toptal.services;

import com.toptal.models.Account;
import com.toptal.models.Jogging;
import com.toptal.repositories.JoggingRepository;
import jdk.nashorn.internal.scripts.JO;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.orm.hibernate3.SessionFactoryUtils;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by dido on 1/16/16.
 */
@Service
public class JoggingService {

	@Autowired
	SessionFactory sessionFactory;

	@Autowired
	JoggingRepository joggingRepository;

	public Set<Jogging> getByUser(Account account){

		return joggingRepository.findByAccount(account);

	}

	public Jogging save(Jogging jogging, Account account){
		jogging.setAccount(account);
		return joggingRepository.save(jogging);
	}


	public void deleteForUser(Long id, Account account){
		Jogging one = joggingRepository.findOne(id);
		if(one.getAccount().getId() == account.getId()){
			joggingRepository.delete(id);
		}

	}

	public Jogging update(Long id,Jogging jogging, Account account){

		Jogging old = joggingRepository.findOne(id);

		if(old.getAccount().getId() == account.getId()){
			return joggingRepository.save(merge(jogging,old));
		}

		return null;

	}

	private Jogging merge(Jogging jogging, Jogging old) {
		if(jogging.getDate() != null){
			old.setDate(jogging.getDate());
		}
		if(jogging.getDescription() != null){
			old.setDescription(jogging.getDescription());
		}
		if(jogging.getDistance() != null){
			old.setDistance(jogging.getDistance());
		}
		if(jogging.getTime() != null){
			old.setTime(jogging.getTime());
		}

		return old;

	}


	public Set<Jogging> findByFromTo(Account account,Date from, Date to){
		Criteria criteria = sessionFactory.openSession().createCriteria(Jogging.class);
		criteria.add(Restrictions.eq("account",account));
		if(from != null){
			criteria = criteria.add(Restrictions.ge("date",from));
		}
		if(to != null){
			criteria = criteria.add(Restrictions.lt("date",to));
		}
		return new HashSet<>(criteria.list());
	}

	public List<Jogging> weekReport(Account account, Date week) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(week);
		Calendar first = (Calendar) calendar.clone();
		first.add(Calendar.DAY_OF_WEEK,
				first.getFirstDayOfWeek() + 1 - first.get(Calendar.DAY_OF_WEEK));
		Calendar last = (Calendar) first.clone();
		last.add(Calendar.DAY_OF_YEAR, 7);
		List<Jogging> report =new ArrayList<>(findByFromTo(account,first.getTime(),last.getTime()));
		if(report.isEmpty()){
			return report;
		}
		Jogging all = new Jogging();
		all.setId(0l);
		all.setDescription("Per Week");
		all.setDate(new Date());
		all.setTime(0l);
		all.setDistance(0.0);
		all.setAccount(account);
		for (Jogging jogging : report) {
			all.setDistance(all.getDistance() + jogging.getDistance());
			all.setTime(all.getTime() + jogging.getTime());
		}
		report.add(all);
		return report;
	}
}
