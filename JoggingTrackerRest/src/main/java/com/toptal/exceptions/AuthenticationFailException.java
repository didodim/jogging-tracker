package com.toptal.exceptions;

/**
 * Created by dido on 08.01.16.
 */
public class AuthenticationFailException extends Exception {

    public AuthenticationFailException(String message) {
        super(message);
    }
}
