package com.toptal.repositories;

import com.toptal.models.Account;
import com.toptal.models.Jogging;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * Created by dido on 15.01.16.
 */
public interface JoggingRepository extends CrudRepository<Jogging, Long> {

    Set<Jogging> findByAccount(@Param("account") Account account);

//    @Query("SELECT p FROM Person p WHERE LOWER(p.lastName) = LOWER(:lastName)")
//    public List<Jogging> findFromTo(Date from, Date to);
}
