package com.toptal.repositories;

import com.toptal.models.Account;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Repository;

/**
 * Created by dido on 07.01.16.
 */

@Repository
@RestResource(path = "accounts")
public interface AccountRepository extends CrudRepository<Account,Long> {
    Account findByEmail(@Param("email") String email);
}
