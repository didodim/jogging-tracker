package com.toptal.repositories;

import com.toptal.models.AccountRole;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RestResource;

import javax.annotation.Resource;

/**
 * Created by dido on 13.01.16.
 */
@RestResource(exported = false)
@Resource
public interface AccountRoleRepository extends CrudRepository<AccountRole,Integer>{
}
