package com.toptal;

import com.toptal.models.Account;
import com.toptal.services.AuthenticationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AdviceMode;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
public class JoggingTrackerRestApplication {

	public static void main(String[] args) {
		SpringApplication.run(JoggingTrackerRestApplication.class, args);

	}


}
