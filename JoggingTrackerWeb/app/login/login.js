'use strict';

angular.module('myApp.login', ['ngRoute'])

    .config(['$routeProvider', function($routeProvider) {
        $routeProvider.when('/login', {
            templateUrl: 'login/login.html',
            controller: 'LoginController',isAnonymous:true
        });
    }])
    .controller('LoginController', ['$scope','AuthService','$location', LoginController]);

function LoginController($scope,AuthService,$location) {
    $scope.update = function(user) {
        var response = AuthService.login(user.email,user.password);
        response.then(function successResponse(response) {
            console.log('Successful login email: '+response.data.email)
            $location.path('/home');
        },function errorResponse(response) {
            console.error(response.data)
            alert('Incorrect email or password! ');
            //var data = response.data;
            //var status = response.status;
            //console.log('DATA '+data.id);
            //console.log(status);
        });

    }
}