'use strict';

angular.module('myApp.report', ['ngRoute'])

    .config(['$routeProvider', function($routeProvider) {
        $routeProvider.when('/report', {
            templateUrl: 'report/report.html',
            controller: 'ReportController'
        });
    }])
    .controller('ReportController', ['$scope','UserService','UtilService','$location', ReportController]);

function ReportController($scope,UserService,UtilService,$location) {
    $scope.joggings = [];
    $scope.filterByWeek = function (week){
        UserService.weekReport(UtilService.formatDate(week)).then(function (data) {
            $scope.joggingsSafe = data;

        });
    };

}