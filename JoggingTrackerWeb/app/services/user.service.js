'use strict';
var myApp = angular.module('myApp');
myApp.factory('UserService', ['$http','$q', 'AppProperty', UserService]);
function UserService($http,$q, AppProperty) {
    var userService = {};
    userService.get = function () {
        return $http.get(AppProperty.server.url + AppProperty.user.get).
        success(function (data, status) {
            return {data: data, status: status, success: true};
        }).error(function (data, status) {
            return {data: data, status: status, success: false};
        });
    };

    userService.update = function (account){
        var deferred = $q.defer();
        $http.patch(AppProperty.server.url + AppProperty.user.update,account).
        success(function (data,status) {

            deferred.resolve(data);
        }).error(function (data,status) {
            alert('User already defined')
            deferred.resolve(data);
        });
        return deferred.promise;

    }

    userService.joggings = function (from,to) {
        var path = '';
        if(!angular.isUndefined(from)){
            path+='from='+from;
        }
        if(!angular.isUndefined(to)){
            path+='&to='+to;
        }

        var deferred = $q.defer();
        $http.get(AppProperty.server.url + AppProperty.user.joggings.get+'?'+path).
        success(function (data) {
            deferred.resolve(data);

        }).error(function (data) {
            deferred.resolve(data);
        });
        return deferred.promise;
    };

    userService.deleteJogging = function (id) {

        $http.delete(AppProperty.server.url + AppProperty.user.joggings.delete + '/'+id).
        success(function (data) {

        }).error(function (data) {
        });
    };

    userService.addJogging = function (jogging) {
        var deferred = $q.defer();
        $http.post(AppProperty.server.url + AppProperty.user.joggings.post,jogging).
        success(function (data,status) {
            deferred.resolve({'data':data,'status':status});
        }).error(function (data) {
            alert('Wrong field');

            deferred.resolve({'data':data,'status':status});
        });
        return deferred.promise;
    };

    userService.updateJogging = function (jogging,id) {
        var deferred = $q.defer();
        $http.patch(AppProperty.server.url + AppProperty.user.joggings.update +'/'+id,jogging).
        success(function (data,status) {
            deferred.resolve({'data':data,'status':status});
        }).error(function (data,status) {

            alert('Wrong field');
            deferred.resolve({'data':data,'status':status});
        });
        return deferred.promise;
    };

    userService.weekReport = function (week) {
        var deferred = $q.defer();
        $http.get(AppProperty.server.url + AppProperty.user.joggings.report+'?week='+week).
        success(function (data) {
            deferred.resolve(data);

        }).error(function (data) {
            deferred.resolve(data);
        });
        return deferred.promise;
    };

    return userService;
}