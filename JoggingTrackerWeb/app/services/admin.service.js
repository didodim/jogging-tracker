'use strict';
var myApp = angular.module('myApp');
myApp.factory('AdminService', ['$http','$q', 'AppProperty', AdminService]);
function AdminService($http,$q, AppProperty) {
    var adminService = {};
    adminService.accounts = function () {
        var deferred = $q.defer();

        $http.get(AppProperty.server.url + AppProperty.admin.accounts).success(function (data,status) {
            deferred.resolve({body:data,status:status});
        }).error(function (data,status) {
            deferred.resolve({body:data,status:status});
        });
        return deferred.promise;
    };

    adminService.rolesByEmail = function (email) {
        var deferred = $q.defer();
        $http.get(AppProperty.server.url + AppProperty.admin.rolesByEmail+'?email='+email).success(function (data,status) {
            deferred.resolve({body:data,status:status});
        }).error(function (data,status) {
            deferred.resolve({body:data,status:status});
        });
        return deferred.promise;
    };

    adminService.promote = function (email,roleType) {
        var deferred = $q.defer();
        $http.get(AppProperty.server.url + AppProperty.admin.promote+'?email='+email+'&roleType='+roleType).success(function (data,status) {
            deferred.resolve({body:data,status:status});
        }).error(function (data,status) {
            deferred.resolve({body:data,status:status});
            alert('Cannot promote');
        });
        return deferred.promise;
    };

    adminService.demote = function (email,roleType) {
        var deferred = $q.defer();
        $http.get(AppProperty.server.url + AppProperty.admin.demote+'?email='+email+'&roleType='+roleType).success(function (data,status) {
            deferred.resolve({body:data,status:status});
        }).error(function (data,status) {
            deferred.resolve({body:data,status:status});
            alert('Cannot demote');
        });
        return deferred.promise;
    };

    return adminService;
}