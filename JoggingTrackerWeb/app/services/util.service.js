/**
 * Created by dido on 18.01.16.
 */
'use strict';
var myApp = angular.module('myApp');
myApp.factory('UtilService', [UtilService]);
function UtilService() {
    var utilService = {};
    utilService.formatDate = function (d) {
        if (angular.isUndefined(d)) {
            return d;
        }

        var curr_date = d.getDate();
        var curr_month = d.getMonth();
        curr_month++;
        var curr_year = d.getFullYear();
        return curr_year + "-" + curr_month + "-" + curr_date;
    };
    utilService.checkNumber = function ($data) {

        if (angular.isUndefined($data)) {
            return 'Cannot be empty';
        }
        if (!angular.isNumber(+($data))) {
            return 'Should be number';
        }
    };
    return utilService;
}
