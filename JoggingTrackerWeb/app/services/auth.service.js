'use strict';
var myApp = angular.module('myApp');
myApp.factory('AuthService', ['$http', 'AppProperty','UserService','AdminService','$location', AuthService]);
function AuthService($http, AppProperty,UserService, AdminService,$location) {
    var authService = {};
    authService.login = function (email, password) {
        return $http.post(AppProperty.server.url + AppProperty.auth.login, {
            'email': email,
            'password': password
        }).
        success(function (data, status) {
            return {data: data, status: status, success: true};
        }).error(function (data, status) {
            return {data: data, status: status, success: false};
        });
    };

    authService.register = function (email, password,fullName) {
        return $http.post(AppProperty.server.url + AppProperty.auth.register, {
            'email': email,
            'password': password,
            'fullName' : fullName
        }).
        success(function (data, status) {
            return {data: data, status: status, success: true};
        }).error(function (data, status) {
            return {data: data, status: status, success: false};
        });
    };

    authService.isLogin = UserService.get;

    authService.isAdmin = AdminService.accounts;

    authService.logout = function () {
        return $http.delete(AppProperty.server.url + AppProperty.auth.logout).
        success(function (data, status) {
            return {data: data, status: status, success: true};
        }).error(function (data, status) {
            return {data: data, status: status, success: false};
        });
    };


    return authService;
}