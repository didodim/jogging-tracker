'use strict';

// Declare app level module which depends on views, and components
angular.module('myApp', [
    'ngRoute',
    'smart-table',
    'xeditable',
    'myApp.home',
    'myApp.tracker',
    'myApp.login',
    'myApp.register',
    'myApp.logout',
    'myApp.report',
    'myApp.admin'
]).run(function(editableOptions) {
    editableOptions.theme = 'bs3'; // bootstrap3 theme. Can be also 'bs2', 'default'
}).
config(['$httpProvider', function ($httpProvider) {
    $httpProvider.defaults.withCredentials = true;
    $httpProvider.defaults.headers.post['Content-Type'] = 'application/json;charset=utf-8';
}]).
config(['$routeProvider', function ($routeProvider) {
    $routeProvider.otherwise({redirectTo: '/home'});
}]).
controller('HeaderController', ['$rootScope', '$scope', 'AuthService', '$location', HeaderController]);

function HeaderController($rootScope, $scope, AuthService, $location) {

    $rootScope.$on('$routeChangeStart', function (event, next) {
        $scope.isAdminHide = true;
        AuthService.isLogin().then(function successResponse(response) {
            $scope.showLogin=false;
            $rootScope.user=response.data;
            AuthService.isAdmin().then(function(data){
                if(data.status == 200){
                    $scope.isAdminHide = false;
                }
            });
            if(next.isAnonymous){
                $location.path('/home');
            }
        },function errorResponse(response) {
            $scope.showLogin=true;
            if(!next.isAnonymous){
                $location.path('/login');
            }
        });




    });

    $scope.isActive = function (viewLocation) {
        return viewLocation === $location.path();
    };
}
