'use strict';
angular.module('myApp').
    constant('AppProperty',{
    server: {
        url: 'http://jogging.int/rest/'
    },
    auth:{
        login: 'auth/login',
        register: 'auth/register',
        logout:'auth/logout'
    },
    user:{
        get:'user',
        update: 'user',
        joggings: {
            get : 'user/joggings',
            delete : 'user/joggings',
            post : 'user/joggings',
            update : 'user/joggings',
            report : 'user/joggings/report'
        }
    },
    admin:{
        accounts:'accounts',
        rolesByEmail:'admin/rolesByEmail',
        promote:'admin/promote',
        demote:'admin/demote'
    },
    role:{
        admin: 'ROLE_ADMIN',
        manager: 'ROLE_USER_MANAGER',
        user: 'ROLE_USER'
    }

});