'use strict';

angular.module('myApp.register', ['ngRoute'])

    .config(['$routeProvider', function($routeProvider) {
        $routeProvider.when('/register', {
            templateUrl: 'register/register.html',
            controller: 'RegisterController',isAnonymous:true
        });
    }])
    .controller('RegisterController', ['$scope','AuthService','$location', RegisterController]);

function RegisterController($scope,AuthService,$location) {
    $scope.update = function(user) {
        var response = AuthService.register(user.email,user.password,user.fullName);
        response.then(function successResponse(response) {

            $location.path('/home');
        },function errorResponse(response) {
            console.error(response.data)
            alert('Email is already registed');

        });

    }
}