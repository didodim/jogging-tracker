'use strict';

angular.module('myApp.tracker', ['ngRoute'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/tracker', {
            templateUrl: 'tracker/tracker.html',
            controller: 'TrackerController'
        });
    }])
    .controller('TrackerController', ['$scope', 'UserService', 'UtilService', '$filter', TrackerController]);

function TrackerController($scope, UserService, UtilService, $filter) {
    $scope.joggings = [];

    $scope.refresh = function (filter) {
        if (angular.isUndefined(filter)) {
            UserService.joggings().then(function (data) {
                $scope.joggingsSafe = data;

            });
        } else {
            UserService.joggings(UtilService.formatDate(filter.from),
                UtilService.formatDate(filter.to)).then(function (data) {
                $scope.joggingsSafe = data;

            });
        }
    }

    $scope.refresh($scope.filter);

    $scope.deleteJogging = function removeItem(jogging) {
        var index = $scope.joggingsSafe.indexOf(jogging);
        if (index !== -1) {
            $scope.joggingsSafe.splice(index, 1);
        }
        UserService.deleteJogging(jogging.id);
    };

    $scope.saveOrUpdateJogging = function (jogging, oldJogging) {
        if (!angular.isUndefined(jogging.formattedDate)) {
            jogging.date = new Date(jogging.formattedDate).getTime();
        }
        if (angular.isUndefined(oldJogging.id)) {
            UserService.addJogging(jogging).then(function (data) {
                $scope.refresh($scope.filter);
            });
        } else {
            UserService.updateJogging(jogging, oldJogging.id).then(function (data) {
                $scope.refresh($scope.filter);
            });
        }
    };
    $scope.addJogging = function () {
        $scope.joggingsSafe.push({
            isShown: true
        })
    };

    $scope.checkDescription = function ($data) {
        if (angular.isUndefined($data)) {
            return 'Cannot be empty';
        }

    };

    $scope.checkDate = function ($data) {
        if (angular.isUndefined($data)) {
            return 'Cannot be empty';
        }
        if (!angular.isDate(new Date($data))) {
            return 'Not in correct date format';
        }

    };

    $scope.checkNumber = UtilService.checkNumber;


}
