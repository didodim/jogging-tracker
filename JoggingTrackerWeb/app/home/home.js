'use strict';

angular.module('myApp.home', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/home', {
    templateUrl: 'home/home.html',
    controller: 'HomeController'
  });
}])

.controller('HomeController', ['$scope','UserService',HomeController]);

function HomeController($scope,UserService) {
  UserService.get().then(function successResponse(response) {
    $scope.user=response.data;
  });
  $scope.updateEmail = function(email){
    if(email == $scope.user.email){
      return;
    }

    UserService.update({'email':email}).then(function(data){
      if(angular.isUndefined(data.email)){
        UserService.get().then(function successResponse(response) {
          $scope.user=response.data;
        });
      }
      $scope.user = data;
    });
  }

  $scope.updateName = function(name){

    UserService.update({'fullName':name}).then(function(data){
      $scope.user = data;
    });
  }

}