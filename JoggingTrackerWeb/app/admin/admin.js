'use strict';

angular.module('myApp.admin', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/admin', {
    templateUrl: 'admin/admin.html',
    controller: 'AdminController'
  });
}])

.controller('AdminController', ['$scope','$rootScope','AdminService','AppProperty',AdminController]);

function AdminController($scope,$rootScope,AdminService,AppProperty) {
  $scope.refreshAcc = function(){
    AdminService.accounts().then(function (response) {
      var accounts = response.body._embedded.accounts;
      $scope.accountsSafe = accounts;
      accounts.forEach(function (account){
          if(account.email == $rootScope.user.email){
            account.isDisable = true;
          }
          AdminService.rolesByEmail(account.email).then(function (response){
            var roles =response.body;
            roles.forEach(function (role){
              if(role.roleType == AppProperty.role.admin){
                account.isAdmin=true;
              }
              if(role.roleType == AppProperty.role.manager){
                account.isManager=true;
              }
              if(role.roleType == AppProperty.role.user){
                account.isUser=true;
              }
            });
            $scope.accountsSafe = accounts;
        });
      });


    });
  }

  $scope.refreshAcc();


  $scope.change = function(account,role){
      if(account.isUser){
        AdminService.promote(account.email,role);
      }else{
        AdminService.demote(account.email,role);
      }
  }

}