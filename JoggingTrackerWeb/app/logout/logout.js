'use strict';

angular.module('myApp.logout', ['ngRoute'])
    .config(['$routeProvider', function($routeProvider) {
        $routeProvider.when('/logout', {
            templateUrl: 'logout/logout.html',
            controller: 'LogoutController'
        });
    }])
    .controller('LogoutController', ['$scope','AuthService','$location', LogoutController]);

function LogoutController($scope,AuthService,$location) {


        var response = AuthService.logout();
        response.then(function successResponse(response) {
            $location.path('/login');
        },function errorResponse(response) {
            console.error(response.data)
            alert(response.data.message);
            //var data = response.data;
            //var status = response.status;
            //console.log('DATA '+data.id);
            //console.log(status);
        });

}